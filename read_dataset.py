# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 09:01:30 2019

@author: LENOVO
"""

import os
import cv2
import numpy as np

def get_image_flt(image_path):
    image = cv2.imread(image_path, -1)
    return image.flatten()

def get_subjects(dataset_path):
    return os.listdir(dataset_path)

def get_images(subject_path):
    return os.listdir(subject_path)

def read_data(dataset_path):
    data = np.zeros((13838, 100*100*3))
    labels = dict()
    index = 0
    
    subjects = get_subjects(dataset_path)
    for subject in subjects:
        print(subject)
        subject_path = dataset_path + "\\" + subject
        image_list = get_images(subject_path)
        
        for image in image_list:
            image_path = subject_path + "\\" + image
            data[index, :] = get_image_flt(image_path)
            labels[index] = subject
            index += 1
    
    return data, labels

def array_to_image(image):
    return np.reshape(image, (100,100,3))
    
dataset_path = "E:\\Talentsprint_WE\\face-recognition-using-cnn\\pubfig83"
data, labels = read_data(dataset_path)





